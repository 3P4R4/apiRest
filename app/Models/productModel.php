<?php 

namespace App\Models;
use codeIgniter\Model;

class productModel extends Model{
    protected $table = 'producto';
    protected $primarykey = 'product_id';
    protected $allowedfields = ['nombre_product', 'precio_product'];
}

?>