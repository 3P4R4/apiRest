<?php

namespace App\Controllers;

use CodeIgniter\Restful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\productModel;

class Productos extends ResourceController
{
	use ResposeTrait;
    // Para obtener todos los productos
    // Index (): se utiliza para recuperar todos los productos.
    public function index(){
        $model= new productModel(); //Instanciamos el modelo
        $data = $model->findAll(); // Para buscar todos los productos
        return $this->respond($data);
    }

    // Para obtener un solo producto
    // show (): este método se utiliza para obtener información de un solo producto en la tabla de base de datos.
    public function show($id=null){
        $model= new productModel();
        $data= $model->getWhere(['product_id'=>$id])->getResult();
        // Ahora Validamos
        if ($data) {
            return $this->respond($data);
        }else{
            return $this->failNotFound('No se encuentran los datos con el ID '.$id);
        }
    }

    // Creando Productos
    // create (): este método se utiliza para insertar información del producto en la tabla DB.
    public function create(){
        $model = new productModel();
        // llamamos a preparar los campos
        $data = [
            'nombre_product' => $this->request->getVar('nombre_product'),
            'precio_product' => $this->request->getVar('precio_product')
        ];
        // Insertamos los datos y recibimos una respuesta
        $model->insert($data);
        $response = [
            'status' => 201,
            'error' => null,
            'message' => [
                'success' => 'Datos Guardado'
            ]
            ];
            return $this->respondCreated($response);
    }

    // Actualizando productos
    // update (): se utiliza para validar los datos del formulario del lado del servidor y actualizarlos en la base de datos MySQL.
    public function update($id=null){
        $model = new productModel();
        $input = $this->request->getRawInput();
        $data = [
            'nombre_product' => $input['nombre_product'],
            'precio_product' => $input['precio_product'],
        ];
        $model->update($id, $data);
        $response = [
            'status' => 200,
            'error' => null,
            'message' => [
                'success' => 'Datos Actualizados'
            ]
            ];
            return $this->respond($response);
        }

        // Eliminando Productos
        // delete (): este método se utiliza para eliminar datos de la base de datos MySQL.
        public function delete($id=null){
            $model = new ProductModel();
            $data = $model->find($id);
            if($data){
                $model->delete($id);
                $response = [
                    'status'   => 200,
                    'error'    => null,
                    'messages' => [
                        'success' => 'Datos Eliminados'
                    ]
                ];
                return $this->respondDeleted($response);
            }else{
                return $this->failNotFound('No se encuentran los datos con el ID '.$id);
        }
        }

}
